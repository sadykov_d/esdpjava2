package com.attractor.esdp;

import com.attractor.esdp.service.InitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
@EnableWebSecurity
@EnableScheduling
public class EsdpApplication {
    @Autowired
    InitService initService;
    public static void main(String[] args) {
        SpringApplication.run(EsdpApplication.class, args);
    }

    @Bean
    public void init(){
        initService.init();
    }

}
