package com.attractor.esdp.model;

public enum FamilyMember {
    WIFE("жена"),
    HUSBAND("муж"),
    SON("сын"),
    DAUGHTER("дочь");

    private final String displayName;

    FamilyMember(String displayName) {
        this.displayName = displayName;
    }


    public String getDisplayName() {
        return displayName;
    }
}
