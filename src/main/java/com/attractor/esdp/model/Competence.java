package com.attractor.esdp.model;

import lombok.*;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Competence {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotEmpty(message = "Название компетенции не может быть пустым!")
    @Size(min=2, max=25, message = "Название компетенции должен быть минимум 2,  максимум 25 символов")
    private String name;

    @Column
    @NotEmpty(message = "Описание компетенции не может быть пустым!")
    @Size(min=2, message = "Описание компетенции должен быть минимум 10 символов")
    private String description;

    /*По ТЗ у него должен быть период актуальности*/
    @Column
    //@Pattern(regexp="[1-9]|10", message = "Срок действия компетенции должен быть цифрой(год) с 1 до 10 или пустым(бессрочным)")
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private Integer validationPeriod;

    @ManyToMany(fetch = FetchType.EAGER)
    @ToString.Exclude private Set<Position> positions = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "Employee_has_competence",
            joinColumns =  @JoinColumn(name = "competence_id"),
            inverseJoinColumns =  @JoinColumn(name = "employee_id")
    )
    @ToString.Exclude private Set<Employee> employees = new HashSet<>();


    public Competence(String name, String description, Integer validationPeriod,
                      Set<Position> positions,Set<Employee> employees) {
        this.name = name;
        this.description = description;
        this.validationPeriod = validationPeriod;
        this.positions = positions;
        this.employees =employees;
    }


}