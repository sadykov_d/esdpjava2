package com.attractor.esdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.time.LocalDate;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Passport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotEmpty(message = "Поле не должно быть пустым")

    private String passportNumber;

    @Column
    @NotEmpty(message = "Поле не должно быть пустым")
    private String inn;

    @Column
    @NotNull (message = "Необходимо ввести дату")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate issuedDate;

    @Column
    @NotNull(message = "Необходимо ввести дату")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate expiryDate;

    @OneToOne

    @JoinColumn(name = "employee_id")

    private Employee employee;

    public Passport(String passportNumber, String inn, LocalDate issuedDate, LocalDate expiryDate, Employee employee) {
        this.passportNumber = passportNumber;
        this.inn = inn;
        this.issuedDate = issuedDate;
        this.expiryDate = expiryDate;
        this.employee = employee;
    }
}
