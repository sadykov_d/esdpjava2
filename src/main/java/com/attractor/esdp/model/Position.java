package com.attractor.esdp.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    @NotEmpty(message = "Название позиции не может быть пустым!")
    @Size(min=2, max=25, message = "Название позиции должен быть минимум 2,  максимум 25 символов")
    private String positionTitle;

    @Column
    @NotNull(message = "Статус позиции не может быть пустым")
    private boolean positionStatusIsActive;


    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="competence_position",
                joinColumns=@JoinColumn(name="position_id"),
                inverseJoinColumns=@JoinColumn(name="competence_id"))
    private List<Competence> competences;



    /* Данное поле не нужно
    @OneToOne(mappedBy = "position")
    private Employee employee;
    */

    /*
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "Position_has_competence",
            joinColumns =  @JoinColumn(name = "position_id"),
            inverseJoinColumns =  @JoinColumn(name = "competence_id")
    )
    @ToString.Exclude private Set<Competence> competences= new HashSet<Competence>();
    */
    public Position(String positionTitle, boolean positionStatusIsActive, List<Competence> competences) {
        this.positionTitle = positionTitle;
        this.positionStatusIsActive = positionStatusIsActive;
        this.competences = competences;
    }
}
