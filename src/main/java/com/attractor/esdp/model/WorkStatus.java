package com.attractor.esdp.model;


import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;


@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class WorkStatus {
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private Employee employee;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private LocalDate startDate;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    /*@OneToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;*/

    /*@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    private Employee employee;*/

    public Integer getId() {
        return id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public WorkStatus(LocalDate startDate, LocalDate endDate/*, Employee employee*/) {
        this.startDate = startDate;
        this.endDate = endDate;
        //this.employee = employee;
    }
}
