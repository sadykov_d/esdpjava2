package com.attractor.esdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column

    private String name;

    /* Данное поле не нужно
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department")
    @ToString.Exclude private List<Employee> employee;
    */

}
