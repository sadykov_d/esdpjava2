package com.attractor.esdp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class EmployeeHasCompetenceWithLevel implements Serializable {

    @EmbeddedId
    private EmpHasCompWithLvlID ehclId;

    @Column
    @Enumerated(EnumType.STRING)
    private CompetenceLevel competenceLevel;

    public EmployeeHasCompetenceWithLevel(Employee employee, Competence competence, CompetenceLevel competenceLevel) {
        ehclId = new EmployeeHasCompetenceWithLevel.EmpHasCompWithLvlID();
        ehclId.competenceId = competence.getId();
        ehclId.employeeId = employee.getId();
        this.competenceLevel = competenceLevel;
    }

    @Embeddable
    public static class EmpHasCompWithLvlID implements Serializable {
        public Integer employeeId;
        public Integer competenceId;
    }

}
