package com.attractor.esdp.model;

public enum RestType {
    VACATION ("отпуск"),
    MATERNITY_LEAVE ("декретный отпуск"),
    HOSPITAL_VACATION ("больничный"),
    NON_PAYED_VACATION ("отгул");

    private final String displayName;

    RestType (String displayName) {
        this.displayName = displayName;
    }


    public String getDisplayName() {
        return displayName;
    }


}


