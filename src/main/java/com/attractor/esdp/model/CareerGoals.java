package com.attractor.esdp.model;

import javax.persistence.*;

@Entity
public class CareerGoals {

    @Id
    @GeneratedValue
    private Integer id;


    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public CareerGoals() {
    }

    public CareerGoals(Employee employee) {
        this.employee = employee;
    }

    public Integer getId() {
        return id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
