package com.attractor.esdp.model;

public enum CompetenceLevel {

    BAD("плохо"),
    SATISFACTORLY("удовлетворительно"),
    GOOD("хорошо"),
    EXCELLENT("отлично");

    private final String displayName;

    CompetenceLevel(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
