
package com.attractor.esdp.model;


public enum FamilyStatus {
    MARRIED("замужем/женат"),
    NOT_MARRIED("не замужем/холост"),
    WIDOW_ER("вдова/вдовец");

    private final String displayName;

    FamilyStatus(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}

