package com.attractor.esdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column
    @NotEmpty(message = "Поле не должно быть пустым")
    private String adress;

    @Column
    @NotEmpty(message = "Поле не должно быть пустым")
    private String email;

    @Column
    @NotEmpty(message = "Поле не должно быть пустым")
    private String workNumber;

    @Column
    @NotEmpty(message = "Поле не должно быть пустым")
    private String privateNumber;

    @Column
    @NotEmpty(message = "Поле не должно быть пустым")
    private String homeNumber;

    @Column
    @NotEmpty(message = "Поле не должно быть пустым")
    private String skypeAccount;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employee_id")
    private Employee employee;

}
