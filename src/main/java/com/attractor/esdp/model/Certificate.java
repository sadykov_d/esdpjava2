package com.attractor.esdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Certificate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotEmpty(message = "Название сертификата не может быть пустым!")
    @Size(min=2, max=20, message = "Название сертификата должен быть минимум 2,  максимум 15 символов")
    private String diplomTitle;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Дата получения сертификата не может быть пустым!")
    @Past(message = "Дата получения сертификата не может быть в будущем!")
    private LocalDate fromDate;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")

    private LocalDate untilDate;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    @NotNull(message = "Выберите сотрудника!")
    Employee employee;


    public Certificate(String diplomTitle, LocalDate fromDate, LocalDate untilDate, Employee employee) {
        this.diplomTitle = diplomTitle;
        this.fromDate = fromDate;
        this.untilDate = untilDate;
        this.employee = employee;
    }
}
