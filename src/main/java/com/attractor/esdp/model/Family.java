package com.attractor.esdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Family {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @Column
    private String surname;

    @Column
    private String fatherName;

    @Enumerated(EnumType.STRING)
    @Column
    private FamilyMember status_member;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthdate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public Family(String name, String surname, String fatherName, FamilyMember status_member, LocalDate birthdate, Employee employee) {
        this.name = name;
        this.surname = surname;
        this.fatherName = fatherName;
        this.status_member = status_member;
        this.birthdate = birthdate;
        this.employee = employee;
    }
}
