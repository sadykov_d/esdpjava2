package com.attractor.esdp.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="persistent_logins")
public class PersistentLogin {

    @Column(name = "login")
    private String login;

    @Id
    @Column(name = "series")
    private String series;

    @Column(name = "token")
    private String token;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_used")
    private Date lastUsed;

    public PersistentLogin() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @PrePersist
    protected void onCreate() {
        lastUsed = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastUsed = new Date();
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(Date lastUsed) {
        this.lastUsed = lastUsed;
    }
}
