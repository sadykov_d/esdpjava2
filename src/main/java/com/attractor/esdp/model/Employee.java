package com.attractor.esdp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotEmpty(message = "Поле не должно быть пустым")
    private String name;

    @Column
    @NotEmpty (message = "Поле не должно быть пустым")
    private String surname;

    @Column
    @NotEmpty (message = "Поле не должно быть пустым")
    private String fatherName;

    @Column
    @NotNull(message = "Необходимо выбрать пол сотрудника")
    private Boolean gender;

    @Column
    @NotNull (message = "Необходимо ввести дату рождения")
    @DateTimeFormat(pattern = "yyyy-MM-dd")

    private LocalDate birthdate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id")
    @NotNull(message = "Поле не должно быть пустым")
    private Department department;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "position_id")
    @NotNull(message = "Поле не должно быть пустым")
    private Position position;



    /*Удалил сущность familyStatus, вместо него создал enum FamilyStatus, где указан список типов семей.положения*/
    @Enumerated(EnumType.STRING)
    @Column
    @NotNull (message = "Необходимо выбрать семейное положение")
    private FamilyStatus familyStatus;

    /*Воинский статус поменял на Boolean*/
    @NotNull (message = "Необходимо выбрать статус воинской службы")
    private Boolean militaryStatus;

    @Column
    String photo;


   /*@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    private List<CareerGoals> careerGoals;*/

}

