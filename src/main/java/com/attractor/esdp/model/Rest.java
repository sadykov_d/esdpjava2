package com.attractor.esdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Rest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /*Создал для типов отпуска enum RestType*/
    @Enumerated(EnumType.STRING)
    @Column
    private RestType typeOfRest;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private Employee employee;

   /* public Rest(RestType typeOfRest, LocalDate startDate, LocalDate endDate, Employee employee) {
        this.typeOfRest = typeOfRest;
        this.startDate = startDate;
        this.endDate = endDate;
        this.employee = employee;
    }*/
}
