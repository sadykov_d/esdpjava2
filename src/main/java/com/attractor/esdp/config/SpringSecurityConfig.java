package com.attractor.esdp.config;

import com.attractor.esdp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    @Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;

    @Autowired
    private final PersistentTokenRepository persistentTokenRepository;
    private final UserService userService;
    final DataSource dataSource;
    private final RequestMatcher csrfRequestMatcher = new RequestMatcher() {
        private final RegexRequestMatcher requestMatcher = new RegexRequestMatcher("/processing-url", null);

        @Override
        public boolean matches(HttpServletRequest request) {
            return requestMatcher.matches(request);
        }
    };

    public SpringSecurityConfig(PersistentTokenRepository persistentTokenRepository, UserService userService, DataSource dataSource) {
        this.persistentTokenRepository = persistentTokenRepository;
        this.userService = userService;
        this.dataSource = dataSource;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/h2-console/**");

    }

    /*@Override

    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/","/resources/**","/login/**").permitAll()
                .and()
               .httpBasic()
                .and()
               .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/processing-url")
               .successHandler(customAuthenticationSuccessHandler)
               .usernameParameter("username")
                .passwordParameter("password")
                .and()
                .exceptionHandling().accessDeniedPage("/accessDenied")
                .and()
                .logout()
               .logoutUrl("/logout")
               .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .permitAll().and()
                //.csrf().requireCsrfProtectionMatcher(csrfRequestMatcher);
                  .csrf().disable();


        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.NEVER);

        http
                .rememberMe()
                .tokenRepository(persistentTokenRepository)
                .rememberMeParameter("remember-me-param")
                .rememberMeCookieName("my-remember-me")
                .tokenValiditySeconds(604800);
    }*/

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        // Database authentication
        auth.
                jdbcAuthentication()
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder());


        // In memory authentication
         //auth.inMemoryAuthentication()
         //      .withUser("admin").password("adminpass").roles("ADMIN");
    }

   /* @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/","/login/**").permitAll()
                .antMatchers("/home").hasAnyAuthority()
                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .formLogin()
                .loginPage("/login")
                //.usernameParameter("username")
                //.passwordParameter("password")
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .permitAll();
                //.and()
                //.csrf().requireCsrfProtectionMatcher(csrfRequestMatcher);
        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.NEVER);

        http
                .rememberMe()
                .tokenRepository(persistentTokenRepository)
                .rememberMeParameter("remember-me-param")
                .rememberMeCookieName("my-remember-me")
                .tokenValiditySeconds(604800);
    }*/
   @Override
   protected void configure(HttpSecurity http) throws Exception {

       http
               .csrf()
               .disable()
               .authorizeRequests()
               .antMatchers("/","/img/**","/css/**","/fonts/**").permitAll()
               .antMatchers("/home").authenticated()
               .antMatchers("/search").hasAnyAuthority("ADMIN","HR")
               .anyRequest().authenticated()
               .and()
               .httpBasic()
               .and()
               .formLogin()
               .loginPage("/login")
               /*.defaultSuccessUrl("/home", true)*/
               .permitAll()
               .and().exceptionHandling().accessDeniedPage("/accessDenied");


       http
               .rememberMe()
               .tokenRepository(persistentTokenRepository)
               .rememberMeParameter("remember-me-param")
               .rememberMeCookieName("my-remember-me")
               .tokenValiditySeconds(604800);
   }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Value("${project.password.encoder.strength}")
    private int strength;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(strength);
    }

    /*@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authenticationService);
    }*/







}
