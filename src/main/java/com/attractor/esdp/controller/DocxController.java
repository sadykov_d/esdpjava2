package com.attractor.esdp.controller;

import com.attractor.esdp.service.DocxFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.print.PrintException;
import java.io.FileNotFoundException;

@RestController
public class DocxController {

    @Autowired
    private DocxFileService docxFileService;

    @GetMapping("/doc/contract/{empId}")
    public void getDocContract(@PathVariable("empId") Integer empId) throws FileNotFoundException, PrintException {
        docxFileService.getDocContract(empId);
    }

    @GetMapping("/doc/intership/{empId}")
    public void getDocIntership(@PathVariable("empId") Integer empId) throws FileNotFoundException, PrintException {
        docxFileService.getDocIntership(empId);
    }

}
