package com.attractor.esdp.controller;

import com.attractor.esdp.dao.*;
import com.attractor.esdp.model.Certificate;
import com.attractor.esdp.model.Competence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/competence")
public class CompetenceController {

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }


    @Autowired
    CompetenceRepository competenceRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    DepartmentRepository departmentRepository;
/*
    @RequestMapping(value = "/listCompetence")
    public String listCompetence(Model model){
        model.addAttribute("competences", competenceRepository.findAll());
        return "viewListOfCompetence";
    }*/

    @RequestMapping(value = "/addForm")
    public String addCompetence(Competence competence, Model model, Principal principal){
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("competence", competence);
        model.addAttribute("competencelist", competenceRepository.findAll());
        return "addCompetenceForm";
}

    @PostMapping(value = "/addForm")
    public String createOrUpdateCompetence(@Valid Competence competence, BindingResult bindingResult, Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        if (bindingResult.hasErrors()) {
           // System.out.println("BINDING RESULT ERROR");
            //return "addCertificateForm";

            model.addAttribute("competence", competence);
            model.addAttribute("competencelist", competenceRepository.findAll());
            return "addCompetenceForm";
        }

            competenceRepository.save(competence);
            model.addAttribute("competencelist", competenceRepository.findAll());
            //return "redirect:/employee/commonInfo/" + cert.getEmployee().getId();
        return "viewListOfCompetence";

    }
}
