package com.attractor.esdp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
public class MainController {

    @RequestMapping(value = {"/", "/login"})
    public String homePage(Principal principal) {
       /*if (SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
         return "login";
        } else {
            return "redirect:/home";
        }*/
        if (principal != null) {
            return "redirect:home";
        }
        return "login";


    }


    @GetMapping(value = {"/accessDenied"})
    public String accessDenied() {
        return "403";
    }
}
