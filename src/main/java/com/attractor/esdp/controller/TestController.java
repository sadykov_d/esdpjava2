package com.attractor.esdp.controller;

import com.attractor.esdp.model.Employee;
import com.attractor.esdp.model.WorkStatus;
import com.attractor.esdp.service.BirthDateService;
import com.attractor.esdp.service.EmployeeDismissalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Map;

@RestController
public class TestController {

    @Autowired
    BirthDateService birthDateService;

    @Autowired
    EmployeeDismissalService employeeDismissalService;

    @GetMapping("/birthdate")
    public Map<Employee, LocalDate> getEmployeesBirthdate(){
       return birthDateService.birthdateList();
    }

    @GetMapping("/anniversary")
    public Map<Employee, LocalDate> getEmployeesAniversary(){ return birthDateService.employeesAnniversary();}

    @GetMapping("/empStatus/{id}")
    public String getEmpStatus(@PathVariable("id") Integer id){
        if(employeeDismissalService.getEmployeeStatus(id))
            return "Сотрудник работает";

        return "Сотрудник не работает";
    }


}
