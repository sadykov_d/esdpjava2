package com.attractor.esdp.controller;

import com.attractor.esdp.dao.*;
import com.attractor.esdp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;


@Controller
public class PassportController {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    PassportRepository passportRepository;

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    CompetenceRepository competenceRepository;

    @Autowired
    CertificateRepository certificateRepository;

    @Autowired
    FamilyRepository familyRepository;

    @Autowired
    UserRepository userRepository;



    /*Форма для добавления/изменения пользователя*/
    @RequestMapping(value = "/addPassportForm")
    public String addEmployeeForm(Model model, Principal principal){
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("passport", new Passport());
        model.addAttribute("employee", employeeRepository.findMaxId());


        return "addPassportForm";

    }

    @PostMapping("/addPassportForm")
    public String createOrDeleteEmployeeController(@Valid Passport passport, BindingResult bindingResult, Model model, Principal principal){
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());


        if (bindingResult.hasErrors()) {
            model.addAttribute("passport", passport);
            model.addAttribute("employee", employeeRepository.findMaxId());
            return "addPassportForm";
        }
        passportRepository.save(passport);
        return "redirect:/addContactForm";

    }






}
