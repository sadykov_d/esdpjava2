package com.attractor.esdp.controller;


import com.attractor.esdp.dao.CertificateRepository;
import com.attractor.esdp.dao.ContactRepository;
import com.attractor.esdp.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

@Controller
public class EmailController {
    @Autowired
    EmailService emailService;
    @Autowired
    ContactRepository contactRepository;
    @Autowired
    CertificateRepository certificateRepository;


    @ResponseBody
    @RequestMapping("/sendEmail")
    public String sendSimpleEmail(@RequestParam("day") Integer day,
                                  @RequestParam("month") Integer month) {
        List<String> emails = contactRepository.getEmailByEmployeeBirthdate(day, month);
        for (String email : emails) {
            emailService.sendSimpleEmail("Салам", email);
        }
        return "Email Sent!";
    }



    @ResponseBody
    @RequestMapping("/sendEmailCertificate")
    public String sendEmailCertificate(@RequestParam("date") String date) {
        List<String> emails = certificateRepository.getEmailByEmployeeCertificate(date);
        for (String email : emails) {
            emailService.sendSimpleEmail("Салам", email);
        }
        return "Email Sent!";
    }

}
