package com.attractor.esdp.controller;

import com.attractor.esdp.dao.*;
import com.attractor.esdp.model.Employee;
import com.attractor.esdp.model.FamilyStatus;
import com.attractor.esdp.model.WorkStatus;
import com.attractor.esdp.service.EmployeeDismissalService;
import com.attractor.esdp.service.PhotoUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.task.DelegatingSecurityContextAsyncTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.util.List;

@Controller
public class EmployeeController {

    private EmployeeRepository employeeRepository;
    private PassportRepository passportRepository;
    private ContactRepository contactRepository;
    private PositionRepository positionRepository;
    private DepartmentRepository departmentRepository;
    private CompetenceRepository competenceRepository;
    private CertificateRepository certificateRepository;
    private FamilyRepository familyRepository;
    private PhotoUploadService photoUploadService;
    private UserRepository userRepository;
    private WorkStatusRepository workStatusRepository;
    private RestRepository restRepository;

    @Autowired
    public EmployeeController(EmployeeRepository employeeRepository,
                              PassportRepository passportRepository,
                              ContactRepository contactRepository,
                              PositionRepository positionRepository,
                              DepartmentRepository departmentRepository,
                              CompetenceRepository competenceRepository,
                              CertificateRepository certificateRepository,
                              FamilyRepository familyRepository,
                              PhotoUploadService photoUploadService,
                              UserRepository userRepository,
                              WorkStatusRepository workStatusRepository,
                              RestRepository restRepository) {

        this.employeeRepository = employeeRepository;
        this.passportRepository = passportRepository;
        this.contactRepository = contactRepository;
        this.positionRepository = positionRepository;
        this.departmentRepository = departmentRepository;
        this.competenceRepository = competenceRepository;
        this.certificateRepository = certificateRepository;
        this.familyRepository = familyRepository;
        this.photoUploadService = photoUploadService;
        this.userRepository = userRepository;
        this.workStatusRepository = workStatusRepository;
        this.restRepository = restRepository;

    }

    @Autowired
    EmployeeDismissalService employeeDismissalService;

    /*Поиск сотрудника*/
    @GetMapping("/search")
    public String searchEmployee(@ModelAttribute String surname,
                                 String name, String fathername, Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("name", name);
        model.addAttribute("surname", surname);
        model.addAttribute("fathername", fathername);

        return "searchEmployee";
    }

   @GetMapping("/employee/commonInfo/{id}")
    public String getCommonInfoById(@PathVariable("id") Integer id, Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());

if (employeeRepository.findById(id).isPresent()) {
    model.addAttribute("employee", employeeRepository.findById(id).get());
            System.out.println(employeeRepository.findById(id).get().getPhoto());

            return "commonInfo";
        } else {
    model.addAttribute("employee", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
            return "redirect:/home";
        }
    }

   /* @PostMapping(path = "/employee/commonInfo/{id}")
    public String EmployeerFired(@PathVariable("id") Integer id, Model model, Principal principal, Integer ids) {
        model.addAttribute("currentuser", employeeRepository
                .findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("employee", employeeRepository.findById(ids).get());
        employeeDismissalService.employeeDismissal(ids, LocalDate.now());
        return "403";
    }*/

    /*общие сведения сотрудника или нескольких сотрудников*/
    @GetMapping("/employee/commonInfo")
    public String getCommonInfo(@RequestParam("name") String name,
                                @RequestParam("surname") String surname,
                                @RequestParam("fathername") String fatherName,
                                Model model, Principal principal){
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        List<Employee> employeeList = employeeRepository
                .findAllByNameContainingIgnoreCaseAndSurnameContainingIgnoreCaseAndFatherNameContainingIgnoreCase
                        (name, surname, fatherName);



        System.out.println("size: " + employeeList.size());
        if (!employeeList.isEmpty())
            System.out.println("size: " + employeeList.get(0).toString());


        else {
            model.addAttribute("notFound", true);
            return "searchEmployee";
        }


        if (employeeList.size() == 1) {
            model.addAttribute("employee", employeeList.get(0));
            return "commonInfo";
        } else {
            model.addAttribute("employeeList", employeeList);
            return "employeeList";
        }

    }

    /*паспортные данные сотрудника*/
    @GetMapping("/employee/passportInfo/{id}")
    public String getPassportInfo(@PathVariable("id") Integer id, Model model, Principal principal){
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("passport",passportRepository.findByEmployee_Id(id));

        return "passport";

    }

    /*контактные данные сотрудника*/
    @GetMapping("/employee/contactInfo/{id}")
    public String getContactInfo(@PathVariable("id") Integer id, Model model,Principal principal){
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("contact",contactRepository.findByEmployee_Id(id));

        return "contact";

    }

    /*компетенции сотрудник*/
    @GetMapping("/employee/competenceInfo/{id}")
    public String getCompetenceInfo(@PathVariable("id") Integer id, Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("employee", employeeRepository.findById(id).get());
        model.addAttribute("competence", competenceRepository.competenceListById(id));

        return "competence";

    }


    /*сертификаты сотрудника*/
    @GetMapping("/employee/certificateInfo/{id}")
    public String getCertificateInfo(@PathVariable("id") Integer id, Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("employee", employeeRepository.findById(id).get());
        model.addAttribute("certificate", certificateRepository.findAllByEmployeeId(id));

        return "certificate";

    }


    /*больничный/отпуск сотрудника*/
    @GetMapping("/employee/restInfo/{id}")
    public String getRestInfo(@PathVariable("id") Integer id, Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("employee", employeeRepository.findById(id).get());
        model.addAttribute("rest", restRepository.findAllByEmployeeId(id));

        return "rest";

    }

    /*семья сотрудника*/
    @GetMapping("/employee/familyInfo/{id}")
    public String getFamilyInfo(@PathVariable("id") Integer id, Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("employee", employeeRepository.findById(id).get());
        model.addAttribute("family", familyRepository.findAllByEmployeeId(id));

        return "family";

    }

    @GetMapping("employee/findByDepId/{id}")
    public String getEmployeeByDepartmentId(@PathVariable("id") Integer id, Model model) {

        model.addAttribute("employeeList", employeeRepository.findAllByDepartmentId(id));

        return "employeeList";

    }

    /*Форма для добавления/изменения пользователя*/
    @RequestMapping(value = "/addEmployeeForm")
    public String addEmployeeForm(Model model, Principal principal){
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("employee", new Employee());
        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("position", positionRepository.findAll());
        model.addAttribute("familyStatus",FamilyStatus.values());

        return "addEmployeeForm";

    }

    @PostMapping("/addEmployeeForm")
    public String createOrDeleteEmployeeController(@Valid Employee employee, BindingResult bindingResult, Model model, Principal principal){
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        if (bindingResult.hasErrors()) {
            model.addAttribute("employee", employee);
            model.addAttribute("department", departmentRepository.findAll());
            model.addAttribute("position", positionRepository.findAll());
            model.addAttribute("familyStatus",FamilyStatus.values());
            return "addEmployeeForm";
        }
        employeeRepository.save(employee);

        workStatusRepository.save(new WorkStatus(LocalDate.now(), null/*, employee*/));

        return "redirect:/addPassportForm";

    }



    /*@PostMapping("/CrUpEmployee")
    public String createOrDeleteEmployeeController(@Valid Employee employee){

        employeeRepository.save(employee);
        return "redirect:/employee/commonInfo/" + employee.getId();

    }*/

    @PutMapping("/employee/updateEmployee")
    public Employee updateEmployee(@RequestBody Employee employee){
        /*
        Сперва получить html страничку, далее из нее вытаскивать данные для нового Employee
        собрать его и сохранить в БД
        employeeRepository.findById(employee.getId()).get().builder()
                .fullName(employee.getFullName())
                .contact(employee.getContact())
                .position(employee.getPosition().getId() )
        */
        return employee;
    }

    @GetMapping("/uploadFileForm")
    public String uploadPhotoForm (Principal principal, Model model) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());

        return "photoUploadForm";

    }

    @PostMapping("/upload-file")
    public String uploadFile(@RequestParam("file") MultipartFile file) throws Exception {

        Integer lastEmployeeId = employeeRepository.findAll().size();
        Employee lastEmployee = employeeRepository.findById(lastEmployeeId).get();
        photoUploadService.uploadEmployeePhoto(file, lastEmployee);

        return "redirect:employee/commonInfo/" + lastEmployee.getId();

    }


}
