package com.attractor.esdp.controller;

import com.attractor.esdp.dao.EmployeeRepository;
import com.attractor.esdp.dao.UserRepository;
import com.attractor.esdp.service.PrinterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PrinterController {

    @Autowired
    private PrinterService printerService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

// http://localhost:8080/print/1/2?docName=contract  Prints contract document
// http://localhost:8080/print/1/2?docName=intership  Prints intership document
/*
    @GetMapping("/print/{empId}/{copies}")
    public String printDoc(@PathVariable("empId") Integer empId,
                           @PathVariable("copies") Integer copies,
                           @RequestParam("docName") String docName) throws FileNotFoundException {
        return printerService.printDoc(empId, copies, docName);
    }*/
}
