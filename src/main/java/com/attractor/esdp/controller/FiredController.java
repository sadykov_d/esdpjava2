package com.attractor.esdp.controller;

import com.attractor.esdp.dao.EmployeeRepository;
import com.attractor.esdp.dao.UserRepository;
import com.attractor.esdp.dao.WorkStatusRepository;
import com.attractor.esdp.model.Employee;
import com.attractor.esdp.model.WorkStatus;
import com.attractor.esdp.service.EmployeeDismissalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.time.LocalDate;
import java.util.List;

@Controller
//@RequestMapping("/fired")
public class FiredController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    EmployeeDismissalService employeeDismissalService;

    @Autowired
    WorkStatusRepository workStatusRepository;


   @PostMapping("/fired")
    public String EmployeerFired(Model model, Principal principal, Integer id) {

        model.addAttribute("currentuser", employeeRepository
                .findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
       model.addAttribute("employee", employeeRepository.findById(id).get());
        employeeDismissalService.employeeDismissal(id, LocalDate.now());
        return "redirect:/employee/commonInfo/" + employeeRepository.findById(id).get().getId();
    }

    @GetMapping("/firedlist")
    public String EmployeeFiredList(Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository
                .findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("fireduser", employeeRepository.findAllEmployeeByWorkStatusEndDateNotNull());
        return "firedlist";
    }

    @GetMapping("/employeelist")
    public String EmployeeList(Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository
                .findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("workeduser", employeeRepository.findAllEmployeeByWorkStatusEndDateIsNull());
        return "emplist";
    }

}
