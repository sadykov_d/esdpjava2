package com.attractor.esdp.controller;

import com.attractor.esdp.dao.*;
import com.attractor.esdp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;


@Controller
public class CertificateController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CertificateRepository certificateRepository;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    DepartmentRepository departmentRepository;

    /*Форма для добавления/изменения пользователя*/
    @RequestMapping("/addCertificateForm/{id}")
    public String addEmployeeForm(Model model, @PathVariable("id") Integer id, Principal principal){

        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());

        model.addAttribute("certificate", new Certificate());

        model.addAttribute("employee", employeeRepository.findById(id).get());

        return "addCertificateForm";

    }


    @PostMapping("/addCertificateForm/{id}")
    public String createOrDeleteEmployeeController(Certificate certificate, BindingResult bindingResult, Model model,
                                                   @PathVariable("id") Integer id, Principal principal){

        Employee emp = employeeRepository.findById(id).get();

        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        if (bindingResult.hasErrors()) {
            model.addAttribute("certificate", certificate);
            model.addAttribute("employee", emp);
            return "addCertificateForm";
        }
        Certificate сertificate1 = new Certificate();
        сertificate1.setEmployee(emp);
        сertificate1.setDiplomTitle(certificate.getDiplomTitle());
        сertificate1.setFromDate(certificate.getFromDate());
        сertificate1.setUntilDate(certificate.getUntilDate());
        certificateRepository.save(сertificate1);
        return "redirect:/employee/certificateInfo/" + emp.getId();

    }

}
