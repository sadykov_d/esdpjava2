package com.attractor.esdp.controller;

import com.attractor.esdp.dao.*;
import com.attractor.esdp.model.Competence;
import com.attractor.esdp.model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/position")
public class PositionController {

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    CompetenceRepository competenceRepository;

    @Autowired
    UserRepository userRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    DepartmentRepository departmentRepository;


    @RequestMapping(value = "/addForm")
    public String addCompetence(Position position, Model model, Principal principal){
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("position", position);
        model.addAttribute("positions", positionRepository.findAll());
        model.addAttribute("competences", competenceRepository.findAll());
        //model.addAttribute("competencelist", competenceRepository.findAll());
        return "addPositionForm";
}
    @PostMapping(value = "/addForm")
    public String createOrUpdateCompetence(@Valid Position position, BindingResult bindingResult, Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        if (bindingResult.hasErrors()) {
           // System.out.println("BINDING RESULT ERROR");
            //return "addCertificateForm";
            model.addAttribute("position", position);
            model.addAttribute("positions", positionRepository.findAll());
            model.addAttribute("competences", competenceRepository.findAll());
            //model.addAttribute("competencelist", competenceRepository.findAll());
            return "addPositionForm";
        }
            position.setPositionStatusIsActive(true);
            positionRepository.save(position);
            model.addAttribute("positions", positionRepository.findAll());
            //return "redirect:/employee/commonInfo/" + cert.getEmployee().getId();
        return "viewListOfPosition";
    }
}
