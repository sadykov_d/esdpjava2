package com.attractor.esdp.controller;

import com.attractor.esdp.dao.*;
import com.attractor.esdp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class ContactController {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    PassportRepository passportRepository;

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    CompetenceRepository competenceRepository;

    @Autowired
    CertificateRepository certificateRepository;

    @Autowired
    FamilyRepository familyRepository;

    @Autowired
    UserRepository userRepository;


    /*Форма для добавления/изменения пользователя*/
    @RequestMapping(value = "/addContactForm")
    public String addEmployeeForm(Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("contact", new Contact());
        model.addAttribute("employee", employeeRepository.findMaxId());


        return "addContactForm";

    }

    @PostMapping("/addContactForm")
    public String createOrDeleteEmployeeController(@Valid Contact contact, BindingResult bindingResult, Model model, Principal principal) {

        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        if (bindingResult.hasErrors()) {
            model.addAttribute("contact", contact);
            model.addAttribute("employee", employeeRepository.findMaxId());
            return "addContactForm";
        }
        contactRepository.save(contact);
        //return "redirect:/employee/contactInfo/" + contact.getId();
        return "photoUploadForm";

    }
}
