package com.attractor.esdp.controller;

import com.attractor.esdp.dao.*;
import com.attractor.esdp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;


import java.security.Principal;


@Controller
public class RestController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private RestRepository restRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    /*Форма для добавления/изменения пользователя*/
    @RequestMapping("/addRestForm/{id}")
    public String addEmployeeForm(Model model, @PathVariable("id") Integer id, Principal principal){

        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());

        model.addAttribute("rest", new Rest());
        model.addAttribute("restList", RestType.values());

        model.addAttribute("employee", employeeRepository.findById(id).get());

        return "addRestForm";

    }


    @PostMapping("/addRestForm/{id}")
    public String createOrDeleteEmployeeController(Rest rest, BindingResult bindingResult, Model model,
                                                   @PathVariable("id") Integer id, Principal principal){

        Employee emp = employeeRepository.findById(id).get();

        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        if (bindingResult.hasErrors()) {
            model.addAttribute("rest", rest);
            model.addAttribute("employee", emp);
            model.addAttribute("restList", RestType.values());
            return "addRestForm";
        }
        Rest rest1 = new Rest();
        rest1.setEmployee(emp);
        rest1.setTypeOfRest(rest.getTypeOfRest());
        rest1.setStartDate(rest.getStartDate());
        rest1.setEndDate(rest.getEndDate());
        restRepository.save(rest1);
        return "redirect:/employee/restInfo/" + emp.getId();

    }

}