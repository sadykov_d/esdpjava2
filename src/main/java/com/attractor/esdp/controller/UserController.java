package com.attractor.esdp.controller;

import com.attractor.esdp.dao.EmployeeRepository;
import com.attractor.esdp.dao.UserRepository;
import com.attractor.esdp.model.Role;
import com.attractor.esdp.model.User;
import com.attractor.esdp.service.EmployeeDismissalService;
import com.attractor.esdp.service.RoleServiceImpl;
import com.attractor.esdp.service.UserService;
import com.attractor.esdp.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    UserServiceImpl userServiceImpl;
    @Autowired
    RoleServiceImpl roleServiceImpl;
    private java.util.List<Role> roleList;

    @Autowired
    EmployeeDismissalService employeeDismissalService;

    @GetMapping("/add")
    public String UserPasswordChangeGet(Model model, Principal principal) {

        model.addAttribute("currentuser", employeeRepository
                .findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("employee", employeeRepository.findAll());
        model.addAttribute("roles",roleServiceImpl.getAll());
        return "useradd";
    }

    @PostMapping("/add")
    public String UserPasswordChange(Model model, Integer user_id, String role, String username, String password, Boolean useractive) {
        model.addAttribute("employee", employeeRepository.findAll());
        model.addAttribute("roles",roleServiceImpl.getAll());
        User user = userRepository.findById(user_id).get();
        roleList.add(roleServiceImpl.getRoleByName(role));
        user.setRole(roleList);
        user.setUsername(username);
        user.setPassword(password);
        user.setEnabled(useractive);
        if (useractive == false){
            employeeDismissalService.employeeDismissal(user_id, LocalDate.now());
        }
        userServiceImpl.update(user);
        return "useradd";
    }


}
