package com.attractor.esdp.controller;

import com.attractor.esdp.dao.DepartmentRepository;
import com.attractor.esdp.dao.EmployeeRepository;
import com.attractor.esdp.dao.UserRepository;
import com.attractor.esdp.model.Department;
import com.attractor.esdp.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/{id}")
    public String getDepById(@PathVariable("id") Integer id, Model model, Principal principal) {
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("departmental", departmentRepository.findById(id).get());
        model.addAttribute("employee", employeeRepository.findAllByDepartmentId(id));

        return "departmentList";
    }

}
