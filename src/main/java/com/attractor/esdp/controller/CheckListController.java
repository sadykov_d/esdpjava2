package com.attractor.esdp.controller;


import com.attractor.esdp.dao.DepartmentRepository;
import com.attractor.esdp.dao.EmployeeRepository;
import com.attractor.esdp.dao.UserRepository;
import com.attractor.esdp.service.CheckListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Controller
public class CheckListController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private CheckListService checkListService;
    @Autowired
    DepartmentRepository departmentRepository;

    private List<String> docList = Arrays.asList("Ксерокопия паспорта",
            "Трудовая книжка", "Фото", "Справка с места жительства", "Справка о несудимости",
            "Военнный билет", "Дипломы и Сертификаты", "Медицинская книжка",
            "Водительское удостоверение", "Свидетельство о браке");
    private Set<String> finalList = new TreeSet<String>();

    @GetMapping("/check")
    public String getDocList(Model model, Principal principal){
        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("documents", this.docList);
        model.addAttribute("selected_docs", this.finalList);
        return "checkList";
    }

    @GetMapping("/check-up")
    public String getFinalDocs(@RequestParam("docName") String docName){

        this.finalList.add(docName);
        return "redirect:/check";

    }

    @GetMapping("/check-del")
    public String getDelDocs(@RequestParam("docName") String docName){

        this.finalList.remove(docName);
        return "redirect:/check";

    }

    @GetMapping("/check-print")
    public String printDocs(){

        checkListService.getCheckDoc(finalList);
        return "redirect:/check";

    }
}