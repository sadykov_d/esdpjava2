package com.attractor.esdp.controller;

import com.attractor.esdp.dao.*;
import com.attractor.esdp.model.Department;
import com.attractor.esdp.model.Employee;
import com.attractor.esdp.service.BirthDateService;
import com.attractor.esdp.service.RoleService;
import com.attractor.esdp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class HomeController {

    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public HomeController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    PassportRepository passportRepository;

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    BirthDateService birthDateService;

    @Autowired
    UserRepository userRepository;


    @GetMapping("/home")
    public String getHomePage(String name, String surname, String fathername, Model model, Principal principal){

        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        /*Дни рождения*/
        //model.addAttribute("birthdateList",birthDateService.birthdateList());
        model.addAttribute("afterBirthDateList",birthDateService.afterBirthDateList());
        model.addAttribute("beforeBirthDateList",birthDateService.beforeBirthDateList());

        /*для поиска*/
        model.addAttribute("name", name);
        model.addAttribute("surname", surname);
        model.addAttribute("fathername", fathername);


        /*для отделов*/
        Map<Department, List<Employee>> employeeMap = new HashMap<>();

        for(Department dep : departmentRepository.findAll()) {
            employeeMap.put(dep,
                    employeeRepository.findAllByDepartmentId(dep.getId()));
        }

        model.addAttribute("department", departmentRepository.findAll());
        model.addAttribute("employee", employeeMap);

        return "homePage2";

    }


}
