package com.attractor.esdp.controller;

        import com.attractor.esdp.dao.*;
        import com.attractor.esdp.model.*;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Controller;
        import org.springframework.ui.Model;
        import org.springframework.validation.BindingResult;
        import org.springframework.web.bind.annotation.*;
        import java.security.Principal;


@Controller
public class FamilyController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FamilyRepository familyRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    /*Форма для добавления/изменения пользователя*/
    @RequestMapping("/addFamilyForm/{id}")
    public String addEmployeeForm(Model model, @PathVariable("id") Integer id, Principal principal){

        model.addAttribute("currentuser", employeeRepository
                .findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());



        model.addAttribute("family", new Family());

        model.addAttribute("employee", employeeRepository.findById(id).get());

        model.addAttribute("familyMember",FamilyMember.values());

        return "addFamilyForm";

    }

    @PostMapping("/addFamilyForm/{id}")
    public String createOrDeleteEmployeeController(Family family, BindingResult bindingResult, Model model,
                                                   @PathVariable("id") Integer id, Principal principal){

        Employee employee = employeeRepository.findById(id).get();

        model.addAttribute("currentuser", employeeRepository.findById(userRepository.findByUsername(principal.getName()).getUser_id()).get());
        model.addAttribute("department", departmentRepository.findAll());

        if (bindingResult.hasErrors()) {
            model.addAttribute("family", family);
            model.addAttribute("employee", employee);
            model.addAttribute("familyMember",FamilyMember.values());
            return "addFamilyForm";
        }
        Family family1 = new Family();
        family1.setEmployee(employeeRepository.findById(id).get());
        family1.setName(family.getName());
        family1.setSurname(family.getSurname());
        family1.setFatherName(family.getFatherName());
        family1.setStatus_member(family.getStatus_member());
        family1.setBirthdate(family.getBirthdate());
        familyRepository.save(family1);
        return "redirect:/employee/familyInfo/" + employee.getId();

    }

}
