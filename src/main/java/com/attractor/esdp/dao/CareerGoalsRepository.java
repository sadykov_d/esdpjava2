package com.attractor.esdp.dao;

import com.attractor.esdp.model.CareerGoals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CareerGoalsRepository extends JpaRepository<CareerGoals, Integer> {}


