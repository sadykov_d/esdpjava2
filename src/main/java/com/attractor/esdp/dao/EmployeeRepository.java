package com.attractor.esdp.dao;

import com.attractor.esdp.model.Employee;
import com.attractor.esdp.model.WorkStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer> {

    List<Employee> findAllByNameContainingIgnoreCaseAndSurnameContainingIgnoreCaseAndFatherNameContainingIgnoreCase
                        (String name,String surname,String fatherName);

    @Query(value = "SELECT * FROM EMPLOYEE where day_of_month(birthdate)=:day and month(birthdate)=:month",
            nativeQuery = true)
    List<Employee> getListEmployeeByBirthdate(@Param("day") Integer day, @Param("month") Integer month);

    @Query(value = "SELECT *  FROM employee e INNER JOIN WORK_STATUS w ON w.id = e.id WHERE w.end_date IS NOT NULL", nativeQuery = true)
    List<Employee> findAllEmployeeByWorkStatusEndDateNotNull();

    @Query(value = "SELECT *  FROM employee e INNER JOIN WORK_STATUS w ON w.id = e.id WHERE w.end_date IS NULL", nativeQuery = true)
    List<Employee> findAllEmployeeByWorkStatusEndDateIsNull();



    List<Employee> findAllByDepartmentId(Integer id);



    @Query(value = "SELECT * FROM EMPLOYEE where id in (select max(id) FROM EMPLOYEE)",
            nativeQuery = true)
    Employee findMaxId();

}
