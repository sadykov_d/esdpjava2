package com.attractor.esdp.dao;

import com.attractor.esdp.model.Competence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompetenceRepository extends JpaRepository<Competence,Integer> {

    @Query(value = "SELECT * from competence where id in (select competence_id from  EMPLOYEE_HAS_COMPETENCE where employee_id=:id)",
    nativeQuery = true)
    List<Competence> competenceListById(@Param("id") Integer id);

}
