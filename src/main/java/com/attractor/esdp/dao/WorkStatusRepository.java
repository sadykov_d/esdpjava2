package com.attractor.esdp.dao;

import com.attractor.esdp.model.Employee;
import com.attractor.esdp.model.WorkStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkStatusRepository extends JpaRepository<WorkStatus, Integer> {

    WorkStatus findByEmployee_Id(Integer id);
    List<WorkStatus> findAllByEndDateIsNotNull();


}
