package com.attractor.esdp.dao;

import com.attractor.esdp.model.Role;
import com.attractor.esdp.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CommonGenericRepository<User>  {


    Optional<User> getByUsername(String username);

    List<User> getUserByRole(Role role);

    User findByUsername(String name);


}
