package com.attractor.esdp.dao;

import com.attractor.esdp.model.Certificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CertificateRepository extends JpaRepository<Certificate,Integer> {

    List<Certificate> findAllByEmployeeId(Integer id);

    @Query(value = "select email from Contact where employee_id in (SELECT employee_id FROM Certificate where until_date in (Cuurent_date)",
            nativeQuery = true)
    List<String> getEmailByEmployeeCertificate(@Param("date") String date);
}
