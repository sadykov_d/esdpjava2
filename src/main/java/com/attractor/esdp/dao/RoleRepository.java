package com.attractor.esdp.dao;

import com.attractor.esdp.model.Role;

public interface RoleRepository extends CommonGenericRepository<Role> {
    Role getByRoleName(String roleName);
}
