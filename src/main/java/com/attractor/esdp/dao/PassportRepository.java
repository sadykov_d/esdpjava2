package com.attractor.esdp.dao;

import com.attractor.esdp.model.Passport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PassportRepository extends JpaRepository<Passport, Integer> {
    Passport findByEmployee_Id(Integer id);

    @Query(value = "select p from Passport p where p.employee.id =:empId")
    Passport getEmployeePassport(Integer empId);
}
