package com.attractor.esdp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CommonGenericRepository<T> extends JpaRepository<T,Integer> {
}
