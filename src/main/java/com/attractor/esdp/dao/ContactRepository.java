package com.attractor.esdp.dao;

import com.attractor.esdp.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact,Integer> {

    Contact findByEmployee_Id(Integer id);

    @Query(value = "select email from Contact where employee_id in (SELECT id FROM EMPLOYEE where day_of_month(birthdate)=day(current_date) and month(birthdate)=month(current_date))",
            nativeQuery = true)
    List<String> getEmailByEmployeeBirthdate(@Param("day") Integer day, @Param("month") Integer month);
}
