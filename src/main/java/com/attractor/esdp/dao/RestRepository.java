package com.attractor.esdp.dao;

import com.attractor.esdp.model.Certificate;
import com.attractor.esdp.model.Rest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RestRepository extends JpaRepository<Rest,Integer> {
    List<Rest> findAllByEmployeeId(Integer id);
}
