package com.attractor.esdp.dao;

import com.attractor.esdp.model.EmployeeHasCompetenceWithLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpHasCompWithLvlRepository extends JpaRepository<EmployeeHasCompetenceWithLevel, Integer> {
}
