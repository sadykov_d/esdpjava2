package com.attractor.esdp.dao;

import com.attractor.esdp.model.Family;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FamilyRepository extends JpaRepository<Family,Integer> {

    List<Family> findAllByEmployeeId(Integer id);

}
