package com.attractor.esdp.service;

import com.attractor.esdp.model.Role;

public interface RoleService extends CommonService<Role> {

    Role getRoleByName(String roleName);

}
