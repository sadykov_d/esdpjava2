package com.attractor.esdp.service;

import org.springframework.stereotype.Service;

import javax.print.*;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.OrientationRequested;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@Service
public class PrinterService {

    public void printDoc(File file) throws FileNotFoundException, PrintException {

        DocFlavor docFlavor=DocFlavor.INPUT_STREAM.AUTOSENSE;
        PrintService [] allPrintServices =
                PrintServiceLookup.lookupPrintServices(docFlavor, null);

        DocAttributeSet docAttributes=
                new HashDocAttributeSet();
        docAttributes.add(OrientationRequested.PORTRAIT);
        PrintRequestAttributeSet printAttributes=
                new HashPrintRequestAttributeSet();
        printAttributes.add(new Copies(1));

        Doc doc=new SimpleDoc(new FileInputStream(file),
                docFlavor,
                null);

        if (allPrintServices.length > 0) {
            DocPrintJob job = allPrintServices[0].createPrintJob();
            job.print(doc,printAttributes);
        }
    }
}

