package com.attractor.esdp.service;

import com.attractor.esdp.model.Role;
import com.attractor.esdp.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface UserService extends CommonService<User> {
    User add(User user);

    void update(User user);

    List<User> getByRole(Role role);



    Optional<User> getByUsername(String username);



}
