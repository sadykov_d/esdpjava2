package com.attractor.esdp.service;

import com.attractor.esdp.dao.CertificateRepository;
import com.attractor.esdp.exceptions.NotFoundCertificateForGivenDateException;
import com.attractor.esdp.model.Certificate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class CertificateUntilDateService {

    @Autowired
    CertificateRepository certificateRepository;

    public List<Certificate> getSixMonthCertificate(){
       return getUntilCertificate(6L);
    }

    public List<Certificate> getOneYearCertificate(){
        return getUntilCertificate(12L);
    }

    public List<Certificate> getUntilCertificate(Long period){

        List<Certificate> list = certificateRepository.findAll();

        List<Certificate> listAfter = new ArrayList<>();


        LocalDate dateNow = LocalDate.now();

        Long daysBefore = 0L;

        //  Long daysBefore = ChronoUnit.DAYS.between(dateNow,date);
        if(period == 14)
            daysBefore = ChronoUnit.DAYS.between(dateNow,dateNow.plus(period, ChronoUnit.DAYS));
        else
            daysBefore = ChronoUnit.DAYS.between(dateNow,dateNow.plus(period, ChronoUnit.MONTHS));


        System.out.println(dateNow);
        System.out.println(daysBefore);
        System.out.println(ChronoUnit.DAYS.between(dateNow,LocalDate.of(2019,9,11)));

        for (Certificate e : list) {

            LocalDate untilUserCertificate = e.getUntilDate();

            Long dif = ChronoUnit.DAYS.between(dateNow,untilUserCertificate);

            if (dif >= 0 && dif <= daysBefore ) {

                listAfter.add(e);

            }
        }

        return listAfter;
    }

}
