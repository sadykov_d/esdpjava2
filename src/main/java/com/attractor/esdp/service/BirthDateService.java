package com.attractor.esdp.service;

import com.attractor.esdp.dao.ContactRepository;
import com.attractor.esdp.dao.EmployeeRepository;
import com.attractor.esdp.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class BirthDateService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    EmailService emailService;


    private static LocalDate today = LocalDate.now();

    private static Integer week = 7;

    public Map<Employee, LocalDate> birthdateList(){

        return  employeeRepository.findAll()
                .stream()
                .filter(emp ->
                        (emp.getBirthdate().getDayOfYear() - today.getDayOfYear()) > -4 &&
                        (emp.getBirthdate().getDayOfYear() - today.getDayOfYear()) <= week ? true : false)
                .collect(Collectors.toMap(employee -> employee, Employee::getBirthdate));

    }

    public List<Employee> afterBirthDateList() {
        List<Employee> employeeList = employeeRepository.findAll();

        List<Employee> employees = new ArrayList<>();

        long now = today.getDayOfYear();

        for (Employee employee :employeeList) {
            LocalDate birthdate = employee.getBirthdate();
            int day = birthdate.getDayOfYear();
            long dif = day - now;
            if (dif >= 0 && dif <= week)
                employees.add(employee);
        }

        return employees;


    }

    public List<Employee> beforeBirthDateList() {
        List<Employee> list = employeeRepository.findAll();

        List<Employee> emp = new ArrayList<>();

        long now = today.getDayOfYear();

        for (Employee employee :list) {
            LocalDate birthdate = employee.getBirthdate();
            int day = birthdate.getDayOfYear();
            long dif = day - now;
            if (dif <= 0 && dif > -7 && day != now)
                emp.add(employee);
        }

        return emp;


    }

    public Map<Employee, LocalDate> employeesAnniversary(){

        return  employeeRepository.findAll()
                .stream()
                .filter(employee -> (today.getYear() - employee.getBirthdate().getYear()) % 5 ==0 ? true : false)
                .collect(Collectors.toMap(employee -> employee, Employee::getBirthdate));

    }


    @Scheduled(cron = " 0 0 8 ? * * ")
    public void sendBirthdayCongs() {
        for (Employee e : employeeRepository.findAll()) {
            if (e.getBirthdate().getMonth().equals(today.getMonth()) &&
                    e.getBirthdate().getDayOfMonth() == today.getDayOfMonth()) {
                String massage = "Уважаемая " + e.getName() + " " + e.getSurname() + ", Ну с ДНЮХОЙ что - ли!!!";
                if (e.getGender().booleanValue())
                    massage = "Уважаемый " + e.getName() + " " + e.getSurname() + ", Ну с ДНЮХОЙ что - ли!!!";

                emailService.sendSimpleEmail(massage,
                        contactRepository.findByEmployee_Id(e.getId()).getEmail());
            }
        }
    }



}
