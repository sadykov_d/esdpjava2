package com.attractor.esdp.service;

import com.attractor.esdp.dao.UserRepository;
import com.attractor.esdp.dao.WorkStatusRepository;
import com.attractor.esdp.model.User;
import com.attractor.esdp.model.WorkStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class EmployeeDismissalService {

    @Autowired
    UserRepository userRepository;

    private WorkStatusRepository workStatusRepository;

    @Autowired
    public EmployeeDismissalService(WorkStatusRepository workStatusRepository) {
        this.workStatusRepository = workStatusRepository;
    }


    public void employeeDismissal (Integer id, LocalDate dismissalDate) {

        WorkStatus workStatusToUpdate = workStatusRepository.findByEmployee_Id(id);

        workStatusToUpdate.setEndDate(dismissalDate);

        User user = userRepository.findById(id).get();
        user.setEnabled(false);
        userRepository.saveAndFlush(user);

        workStatusRepository.save(workStatusToUpdate);

    }

    public WorkStatus getEmployee(Integer id) {

        return workStatusRepository
                .findByEmployee_Id(id);

    }

    public Boolean getEmployeeStatus(Integer id) {

        return workStatusRepository
                .findByEmployee_Id(id)
                .getEndDate() == null ? true : false;

    }



}
