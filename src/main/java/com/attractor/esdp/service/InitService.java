package com.attractor.esdp.service;

import com.attractor.esdp.dao.*;
import com.attractor.esdp.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.Arrays;

@Service
public class InitService {

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    PassportRepository passportRepository;

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    WorkStatusRepository workStatusRepository;

    @Autowired
    RestRepository restRepository;

    @Autowired
    CertificateRepository certificateRepository;

    @Autowired
    CompetenceRepository competenceRepository;

    @Autowired
    FamilyRepository familyRepository;

    @Autowired
    EmpHasCompWithLvlRepository empHasCompWithLvlRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private CareerGoalsRepository careerGoalsRepository;



    public void init() {

        Department depIT = new Department(1,"IT отдел");
        Department depHR = new Department(2,"Отдел кадров");
        Department depMarketing = new Department(3,"Отдел маркетинка");
        Department depSell = new Department(4,"Отдел продаж");
        departmentRepository.saveAll(Arrays.asList(depIT,depHR,depMarketing,depSell));

        Position hr = new Position("Специалист по персоналу",true, null);
        Position head = new Position("Руководитель",true, null);
        Position progger = new Position("Программист",true, null);
        Position marketingMan = new Position("Маркетолого",true, null);
        Position salesManager = new Position("Специалист по продажам",true, null);
        Position web_designer = new Position("Веб-дизайне",true, null);
        positionRepository.saveAll(Arrays.asList(hr,head,progger,marketingMan,salesManager,web_designer));


        Role roleAdmin = new Role("ADMIN");
        Role roleHr = new Role("HR");
        Role roleUser = new Role("USER");
        roleService.add(roleAdmin);
        roleService.add(roleUser);
        roleService.add(roleHr);


        Employee emp1 = new Employee(1,"Эркин", "Касымалиев","Айбекович", true,
                LocalDate.of(1977,10,5),depIT,progger, FamilyStatus.MARRIED,true,
                "/empImages/1KasymalievErkin.jpg");
        Employee emp2 = new Employee(2,"Айгуль", "Кыдыралиева", "Бакытовна", false,
                LocalDate.of(1989,9,28),depHR,hr, FamilyStatus.MARRIED,false,
                "/empImages/2KydyralievaAigul.jpg");
        Employee emp3 = new Employee(3,"Айбек", "Исаков", "Жолдошевич", true,
                LocalDate.of(1974,9,30),depMarketing,marketingMan,
                FamilyStatus.NOT_MARRIED,false, "/empImages/3IsakovAibek.jpg");
        Employee emp4 = new Employee(4,"Роман", "Биркин", "Борисович", true,
                LocalDate.of(1994,10,1),depSell,salesManager,
                FamilyStatus.MARRIED,true, "/empImages/4BirkinRoman.jpg");
        Employee emp5 = new Employee(5,"Екатерина", "Романова", "Георгиевна", false,
                LocalDate.of(1991,9,26),depIT,web_designer,
                FamilyStatus.NOT_MARRIED,false, "/empImages/5RomanovKatya.jpg");
        Employee emp6 = new Employee(6,"Болот", "Ибраимов", "Раимбекович", true,
                LocalDate.of(1999,9,11),depSell,salesManager,
                FamilyStatus.MARRIED,true, "/empImages/6IbragimovBolot.jpg");
        Employee emp7 = new Employee(7,"Болот", "Медеров", "Нурсултанович", true,
                LocalDate.of(1989,11,11),depSell,salesManager,
                FamilyStatus.NOT_MARRIED,true, "/empImages/7MederovBolot.jpg");
        Employee emp8 = new Employee(8,"Александр", "Миркин", "Петрович", true,
                LocalDate.of(1989,9,13),depMarketing,head,
                FamilyStatus.NOT_MARRIED,true, "/empImages/4BirkinRoman.jpg");
        Employee emp9 = new Employee(9,"Айбек", "Казыбеков", "Алиевич", true,
                LocalDate.of(1985,8,5),depIT,head,
                FamilyStatus.NOT_MARRIED,false, "/empImages/5RomanovKatya.jpg");
        Employee emp10 = new Employee(10,"Асель", "Разакова", "Аргеновна", false,
                LocalDate.of(1982,8,4),depHR,head,
                FamilyStatus.MARRIED,false, "/empImages/6IbragimovBolot.jpg");
        Employee emp11 = new Employee(11,"Максим", "Васильев", "Борисович", true,
                LocalDate.of(1983,11,11),depSell,head,
                FamilyStatus.NOT_MARRIED,true, "/empImages/7MederovBolot.jpg");
        employeeRepository.saveAll(Arrays.asList(emp1,emp2,emp3,emp4,emp5,emp6,emp7,emp8,emp9,emp10,emp11));

        User usr1 = new User("erkin","pass1", Arrays.asList(roleService.getRoleByName("ADMIN")), true);
        userService.add(usr1);
        User usr2 = new User("erkin2","pass2",Arrays.asList(roleService.getRoleByName("HR")) ,true);
        userService.add(usr2);
        User usr3 = new User("erkin3","pass2",Arrays.asList(roleService.getRoleByName("USER")), true);
        userService.add(usr3);
        User usr4 = new User("erkin4","pass2",Arrays.asList(roleService.getRoleByName("USER")), true);
        userService.add(usr4);
        User usr5 = new User("erkin5","pass2",Arrays.asList(roleService.getRoleByName("USER")),true);
        userService.add(usr5);
        User usr6 = new User("erkin6","pass2",Arrays.asList(roleService.getRoleByName("USER")), true);
        userService.add(usr6);
        User usr7 = new User("erkin7","pass2",Arrays.asList(roleService.getRoleByName("USER")), false);
        userService.add(usr7);

        Passport passport1 = new Passport("AN1258988","12345689",
                LocalDate.of(2017,12,12),LocalDate.of(2027,12,12),emp1);
        Passport passport2 = new Passport("AN1558988","15898789",
                LocalDate.of(2012,12,12),LocalDate.of(2022,12,12),emp2);
        Passport passport3 = new Passport("AN1258368","11236979",
                LocalDate.of(2013,12,12),LocalDate.of(2013,12,12),emp3);
        Passport passport4 = new Passport("ID1498589","15598775",
                LocalDate.of(2018,12,12),LocalDate.of(2028,12,12),emp4);
        Passport passport5 = new Passport("ID1398589","15697123",
                LocalDate.of(2012,12,12), LocalDate.of(2022,12,12),emp5);
        Passport passport6 = new Passport("ID1293289","12589872",
                LocalDate.of(2014,12,12), LocalDate.of(2024,12,12),emp6);
        Passport passport7 = new Passport("ID1293289","12589872",
                LocalDate.of(2015,12,12), LocalDate.of(2025,12,12),emp7);
        Passport passport8 = new Passport("ID1293289","12589872",
                LocalDate.of(2015,12,12), LocalDate.of(2025,12,12),emp7);
        Passport passport9 = new Passport("ID1293289","12589872",
                LocalDate.of(2015,12,12), LocalDate.of(2025,12,12),emp7);
        Passport passport10 = new Passport("ID1293289","12589872",
                LocalDate.of(2015,12,12), LocalDate.of(2025,12,12),emp7);
        Passport passport11 = new Passport("ID1293289","12589872",
                LocalDate.of(2015,12,12), LocalDate.of(2025,12,12),emp7);
        passportRepository.saveAll(Arrays.asList(passport6,passport5,passport4,passport3,passport2,passport1, passport7,passport8,passport9,passport10,passport11));


        Contact contact1 = new Contact(1,"Sovetskaya,89", "erkin@mail.ru",
                "0312259878","0999068058",
                "0312566689","erkin123",emp1);
        Contact contact2 = new Contact(2,"10 mrk,11", "aigul@mail.ru",
                "0312897888","0770256987",
                "0312566685","aika123",emp2);
        Contact contact3 = new Contact (3,"Druzhba,14", "aiba@mail.ru",
                "0312145698","0555998789",
                "0312566684","aiba123",emp3);
        Contact contact4 = new Contact(4,"Pravda,4", "roman@mail.ru",
                "0312523698","0700258741",
                "0312566681","roma123",emp4);
        Contact contact5 = new Contact(5,"Kievskaya,81", "ekat@mail.ru",
                "0312555879","0550589666",
                "0312566680","katya123",emp5);
        Contact contact6 = new Contact(6,"Isanova,78", "bolot@mail.ru",
                "0312564789","0502477852",
                "0312566688","bolot123",emp6);
        Contact contact7 = new Contact(7,"Isanova,77", "QERTTY@mail.ru",
                "0312564789","0502477852",
                "0312566688","bolot123",emp7);
        Contact contact8 = new Contact(8,"Isanova,77", "QERTTY@mail.ru",
                "0312564789","0502477852",
                "0312566688","bolot123",emp8);
        Contact contact9 = new Contact(9,"Isanova,77", "QERTTY@mail.ru",
                "0312564789","0502477852",
                "0312566688","bolot123",emp9);
        Contact contact10 = new Contact(10,"Isanova,77", "QERTTY@mail.ru",
                "0312564789","0502477852",
                "0312566688","bolot123",emp10);
        Contact contact11 = new Contact(11,"Isanova,77", "QERTTY@mail.ru",
                "0312564789","0502477852",
                "0312566688","bolot123",emp11);
        contactRepository.saveAll(Arrays.asList(contact1,contact2,contact3,contact4,contact5,contact6, contact7,contact8,contact9,contact10,contact11));

        Family familyMember1 = new Family("Вера", "Фармина", "Шариповна", FamilyMember.WIFE,
                LocalDate.of(1982,11,14),emp1);
        Family familyMember2 = new Family("Айбек", "Касымалиев", "Эркинович", FamilyMember.SON,
                LocalDate.of(2002,07,14),emp1);
        Family familyMember3 = new Family("Бред", "Питт", "Владимирович", FamilyMember.HUSBAND,
                LocalDate.of(1985,04,22),emp2);
        Family familyMember4 = new Family("Наталья", "Романофф", "Шаршеновна", FamilyMember.WIFE,
                LocalDate.of(1996,02,19),emp4);
        Family familyMember5 = new Family("Петр", "Биркин","Романович", FamilyMember.SON,
                LocalDate.of(2018,03,28),emp4);
        Family familyMember6 = new Family("Айгуль", "Асанова", "Болотовна", FamilyMember.WIFE,
                LocalDate.of(1996,12,27),emp6);
        familyRepository.saveAll(Arrays.asList(familyMember1,familyMember2,familyMember3,familyMember4,
                familyMember5,familyMember6));

        WorkStatus workStatus1 = new WorkStatus(LocalDate.of(2014,12,12), null/*,emp1*/);
        WorkStatus workStatus2 = new WorkStatus(LocalDate.of(2014,06,05), null/*,emp2*/);
        WorkStatus workStatus3 = new WorkStatus(LocalDate.of(2012,01,22), null/*,emp3*/);
        WorkStatus workStatus4 = new WorkStatus(LocalDate.of(2015,12,11), null/*,emp4*/);
        WorkStatus workStatus5 = new WorkStatus(LocalDate.of(2018,03,19), null/*,emp5*/);
        WorkStatus workStatus6 = new WorkStatus(LocalDate.of(2017,05,12), null/*,emp6*/);
        WorkStatus workStatus7 = new WorkStatus(LocalDate.of(2017,05,12), null/*,emp7*/);
        WorkStatus workStatus8 = new WorkStatus(LocalDate.of(2017,05,12), null/*,emp8*/);
        WorkStatus workStatus9 = new WorkStatus(LocalDate.of(2017,05,12), null/*,emp9*/);
        WorkStatus workStatus10 = new WorkStatus(LocalDate.of(2017,05,12), null/*,emp10*/);
        WorkStatus workStatus11 = new WorkStatus(LocalDate.of(2017,05,12), null/*,emp11*/);
        workStatusRepository.saveAll(Arrays.asList(workStatus1,workStatus2,workStatus3,workStatus4,
                workStatus5,workStatus6,workStatus7,workStatus8,workStatus9,workStatus10,workStatus11));

        Rest rest1 = new Rest(1,RestType.VACATION,LocalDate.of(2015,11,12),
                LocalDate.of(2015,11,26), emp1);
        Rest rest2 = new Rest(2,RestType.NON_PAYED_VACATION,LocalDate.of(2015,05,12),
                LocalDate.of(2015,05,26), emp2);
        Rest rest3 = new Rest(3,RestType.HOSPITAL_VACATION,LocalDate.of(2013,11,12),
                LocalDate.of(2013,11,26), emp3);
        Rest rest4 = new Rest(4,RestType.VACATION,LocalDate.of(2016,11,12),
                LocalDate.of(2016,11,26), emp4);
        Rest rest5 = new Rest(5,RestType.VACATION,LocalDate.of(2018,04,12),
                LocalDate.of(2018,04,26), emp6);
        Rest rest6 = new Rest(6,RestType.VACATION,LocalDate.of(2017,11,12),
                LocalDate.of(2017,11,26), emp3);
        restRepository.saveAll(Arrays.asList(rest1,rest2,rest3,rest4,rest5,rest6));

        Certificate certificate1 = new Certificate("Курсы SQL",
                LocalDate.of(2018,11,11),LocalDate.of(2023,12,11),emp1);
        Certificate certificate2 = new Certificate("Курсы продаж",
                LocalDate.of(2018,11,11),LocalDate.of(2019,9,11),emp4);
        Certificate certificate3 = new Certificate("Курсы маркетинга",
                LocalDate.of(2018,11,11),LocalDate.of(2019,12,11),emp3);
        certificateRepository.saveAll(Arrays.asList(certificate1,certificate2,certificate3));

        Competence competence1 = new Competence(1,"SQL","Владение SQL на высоком уровне",
                5,null,null);
        Competence competence2 = new Competence(2,"Основы маркетинга","базовый курс по маркетингу",
                5,null,null);
        Competence competence3 = new Competence(3,"Основы продаж","базовый курс по продажам",
                4,null,null);
        Competence competence4 = new Competence(4,"Вождение","категория A или B по вождению", 6,
                null,null);
        competenceRepository.saveAll(Arrays.asList(competence1, competence2, competence3, competence4));

        /*Для PositionHasCompetence*/
        //competence1.getPositions().add(progger);
        //competence1.getPositions().add(web_designer);
        //competence2.getPositions().add(marketingMan);
        //competence3.getPositions().add(salesManager);
        //competenceRepository.saveAll(Arrays.asList(competence1,competence2,competence3,competence4));


        /*Для EmployeeHasCompetenceWithLevel*/
        EmployeeHasCompetenceWithLevel e1 = new EmployeeHasCompetenceWithLevel(emp1,competence4,CompetenceLevel.BAD);
        EmployeeHasCompetenceWithLevel e2 = new EmployeeHasCompetenceWithLevel(emp2,competence4,CompetenceLevel.EXCELLENT);
        EmployeeHasCompetenceWithLevel e3 = new EmployeeHasCompetenceWithLevel(emp3,competence4,CompetenceLevel.GOOD);
        EmployeeHasCompetenceWithLevel e4 = new EmployeeHasCompetenceWithLevel(emp4,competence4,CompetenceLevel.SATISFACTORLY);
        EmployeeHasCompetenceWithLevel e5 = new EmployeeHasCompetenceWithLevel(emp5,competence4,CompetenceLevel.GOOD);
        EmployeeHasCompetenceWithLevel e6 = new EmployeeHasCompetenceWithLevel(emp6,competence4,CompetenceLevel.GOOD);
        EmployeeHasCompetenceWithLevel e7 = new EmployeeHasCompetenceWithLevel(emp1,competence1,CompetenceLevel.EXCELLENT);
        EmployeeHasCompetenceWithLevel e8 = new EmployeeHasCompetenceWithLevel(emp5,competence1,CompetenceLevel.SATISFACTORLY);
        EmployeeHasCompetenceWithLevel e9 = new EmployeeHasCompetenceWithLevel(emp3,competence2,CompetenceLevel.GOOD);
        EmployeeHasCompetenceWithLevel e10 = new EmployeeHasCompetenceWithLevel(emp6,competence3,CompetenceLevel.BAD);
        empHasCompWithLvlRepository.saveAll(Arrays.asList(e1,e2,e3,e4,e5,e6,e7,e8,e9,e10));

    }
}
