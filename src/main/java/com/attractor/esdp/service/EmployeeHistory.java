package com.attractor.esdp.service;

import com.attractor.esdp.dao.WorkStatusRepository;
import com.attractor.esdp.model.WorkStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeHistory {

    @Autowired
    private WorkStatusRepository workStatusRepository;

    public WorkStatus empHistory(Integer id) {
        return workStatusRepository.findByEmployee_Id(id);
    }
}
