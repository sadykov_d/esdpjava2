package com.attractor.esdp.service;

import com.attractor.esdp.dao.EmployeeRepository;
import com.attractor.esdp.dao.PassportRepository;
import com.attractor.esdp.model.Employee;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import javax.print.PrintException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Service
public class DocxFileService {

    @Autowired
    private PassportRepository passportRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public void getDocContract(Integer empId) throws FileNotFoundException, PrintException {

        String passportNumber = passportRepository.findByEmployee_Id(empId).getPassportNumber().substring(0,1) +
                " " + passportRepository.findByEmployee_Id(empId).getPassportNumber().substring(2,8);

        Employee employee = employeeRepository.findById(empId).get();

        if (new File("src/main/resources/static/" +
                employee.getName() + "_" + employee.getSurname() + "/contract.docx").exists()){
            new PrinterService().printDoc(new File("src/main/resources/static/" +
                    employee.getName() + "_" + employee.getSurname() + "/contract.docx"));
            return;

        }
            new File("src/main/resources/static/" +
                employee.getName() + "_" + employee.getSurname()).mkdirs();

        try {
            XWPFDocument document = new XWPFDocument();

            XWPFParagraph paragraph = document.createParagraph();
            XWPFRun run = paragraph.createRun();

            // Head
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run.setBold(true);
            run.setFontSize(18);
            run.setText("ТРУДОВОЙ ДОГОВОР");
            paragraph.setSpacingAfter(500);

            paragraph = document.createParagraph();
            run = paragraph.createRun();

            //Present Date
            paragraph.setAlignment(ParagraphAlignment.RIGHT);
            run.setItalic(true);
            run.setText(LocalDate.now().format(DateTimeFormatter
                    .ofPattern("dd MMMM  yyyy", new Locale("ru"))) + "г.");
            paragraph.setSpacingAfter(500);

            //Body. Part #1
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   Компания ");
            run.setBold(true);
            run.setText("\"Finance Soft\" ");
            run.setBold(false);
            run.setText(" именуемый в дальнейшем «Работодатель», с одной стороны, и гр. ");
            run.setBold(true);
            run.setText(employee.getSurname() + " " + employee.getName() + " " + employee.getFatherName());
            run.setBold(false);

            run.setText(", паспорт: № " + passportNumber +
                    ", именуемый в дальнейшем «Работник», с другой стороны, именуемые в дальнейшем «Стороны», " +
                    "заключили настоящий договор, в дальнейшем «Договор», о нижеследующем:");

            //Body. Part #2
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run.setFontSize(14);
            run.setText("1. ПРЕДМЕТ ТРУДОВОГО ДОГОВОРА");

            paragraph.setSpacingBefore(200);
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setBold(false);
            run.setText("1.1. Работник принимается к Работодателю для выполнения работы в должности " +
                    employee.getPosition().getPositionTitle() + " " + employee.getDepartment().getName());
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("1.2. Работник обязан приступить к работе с " + LocalDate.now().format(DateTimeFormatter
                    .ofPattern("dd MMMM  yyyy", new Locale("ru"))) + "г.");
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("1.3. Настоящий трудовой договор вступает в силу с момента подписания " +
                    "его обеими сторонами и заключен на неопределенный срок.");
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("1.4. Работа по настоящему договору является для Работника основной.");

            //Body. Part #3.
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run.setFontSize(14);
            run.setText("2. ПРАВА И ОБЯЗАННОСТИ СТОРОН");

            paragraph.setSpacingBefore(200);
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setBold(false);
            run.setText("2.1. Работник подчиняется непосредственно Генеральному директору.");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("2.2. Работник обязан:");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.2.1. Соблюдать установленные Работодателем Правила внутреннего трудового распорядка, " +
                    "производственную и финансовую дисциплину, добросовестно относиться к исполнению своих должностных обязанностей.");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.2.2. Соблюдать требования охраны труда, техники безопасности и производственной санитарии.");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.2.3. Способствовать созданию на работе благоприятного делового и морального климата.");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("2.3. Работодатель обязуется:");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.3.1. Предоставить Работнику работу в соответствии с условиями настоящего трудового договора. " +
                    "Работодатель вправе требовать от Работника выполнения обязанностей (работ), не обусловленных настоящим " +
                    "трудовым договором, только в случаях, предусмотренных законодательством о труде КР.");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.3.2. Обеспечить безопасные условия работы в соответствии с требованиями " +
                    "Правил техники безопасности и законодательства о труде КР.");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.3.3. Оплачивать труд Работника в размере, установленном в п.3.1. " +
                    "настоящего трудового договора.");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.3.4. Оплачивать в случае производственной необходимости в целях повышения " +
                    "квалификации Работника его обучение.");
            paragraph.setSpacingAfter(500);

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            run.setItalic(true);
            run.setFontSize(11);
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("г. д. Балапанов К. К.                                                  ");
            paragraph.setAlignment(ParagraphAlignment.RIGHT);
            run.setText(employee.getSurname() + " " +
                    employee.getName().substring(0,1) + ". " +
                    employee.getFatherName().substring(0,1) + ".");


            // сохраняем модель docx документа в файл
            FileOutputStream outputStream = new FileOutputStream("src/main/resources/static/" +
                    employee.getName() + "_" + employee.getSurname() + "/contract.docx");
            document.write(outputStream);
            new PrinterService().printDoc(new File("src/main/resources/static/" +
                    employee.getName() + "_" + employee.getSurname() + "/contract.docx"));
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getDocIntership(Integer empId) throws FileNotFoundException, PrintException {

        String passportNumber = passportRepository.findByEmployee_Id(empId).getPassportNumber().substring(0, 1) +
                " " + passportRepository.findByEmployee_Id(empId).getPassportNumber().substring(2, 8);

        Employee employee = employeeRepository.findById(empId).get();

        if (new File("src/main/resources/static/" +
                employee.getName() + "_" + employee.getSurname() + "/intership.docx").exists()){
            new PrinterService().printDoc(new File("src/main/resources/static/" +
                    employee.getName() + "_" + employee.getSurname() + "/intership.docx"));
            return;
    }
        new File("src/main/resources/static/" +
                employee.getName() + "_" + employee.getSurname()).mkdirs();

        try {
            XWPFDocument document = new XWPFDocument();

            XWPFParagraph paragraph = document.createParagraph();
            XWPFRun run = paragraph.createRun();

            // Head
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run.setBold(true);
            run.setFontSize(18);
            run.setText("Договор стажировки");
            paragraph.setSpacingAfter(500);

            paragraph = document.createParagraph();
            run = paragraph.createRun();

            //Present Date
            paragraph.setAlignment(ParagraphAlignment.RIGHT);
            run.setItalic(true);
            run.setText(LocalDate.now().format(DateTimeFormatter
                    .ofPattern("dd MMMM  yyyy", new Locale("ru"))) + "г.");
            paragraph.setSpacingAfter(500);

            //Body. Part #1
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   Компания ");
            run.setBold(true);
            run.setText("\"Finance Soft\" ");
            run.setBold(false);
            run.setText(" именуемый в дальнейшем «Работодатель», с одной стороны, и гр. ");
            run.setBold(true);
            run.setText(employee.getSurname() + " " + employee.getName() + " " + employee.getFatherName());
            run.setBold(false);

            run.setText(", паспорт: № " + passportNumber +
                    ", именуемый в дальнейшем «Стажер», с другой стороны, именуемые в дальнейшем «Стороны», " +
                    "заключили настоящий договор, в дальнейшем «Договор», о нижеследующем:");

            //Body. Part #2
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run.setFontSize(14);
            run.setText("1. ПРЕДМЕТ ДОГОВОРА НА СТАЖИРОВКУ");

            paragraph.setSpacingBefore(200);
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setBold(false);
            run.setText("1.1. Настоящий договор регулирует отношения между Работодателем и Стажером, " +
                    "связанные с обучением последнего профессии " +
                    employee.getPosition().getPositionTitle() + " " + employee.getDepartment().getName());
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("1.2. Настоящий договор не является трудовым, не регламентирует " +
                    "трудовые отношения, а лишь является сопровождающим деятельность для дальнейшего трудоустройства.");

            //Body. Part #3.
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            run.setFontSize(14);
            run.setText("2. Права и обязанности сторон ");

            paragraph.setSpacingBefore(200);
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setBold(false);
            run.setText("2.1. Права и обязанности стажера:");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.1.1. При прохождении обучения знакомиться с условиями работы в организации, " +
                    "нормативными актами, регулирующими данную сферу, правилами охраны труда и т.п. ");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.2.2. При прохождении обучения знакомиться с условиями работы в организации, " +
                    "нормативными актами, регулирующими данную сферу, правилами охраны труда и т.п.");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.2.3. Пройти обучение на должности, предложенной Работодателем.");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("2.2. Права и обязанности работодателя:");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.2.1. Требовать от Стажера добросовестного исполнения " +
                    "обязанностей по настоящему Договору. ");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.2.2. Поощрять Стажера за добросовестное отношение к " +
                    "обучению и эффективный труд при выполнении производственной практики. ");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   2.2.3. Обеспечить Стажера возможностью обучения в соответствии с " +
                    "настоящим Договором, обеспечивать Стажера средствами, материалами и оборудованием, " +
                    "которые необходимы при исполнении им условий настоящего Договора.");

            paragraph.setSpacingBefore(200);
            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setBold(false);
            run.setText("3. Срок действия договора ");

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("   3.1. Настоящий Договор вступает в силу с " +
                    LocalDate.now().format(DateTimeFormatter
                            .ofPattern("dd MMMM  yyyy", new Locale("ru"))) +
                    "г. и действует в течение одного месяца со дня подписания.");
            paragraph.setSpacingAfter(500);

            paragraph = document.createParagraph();
            run = paragraph.createRun();
            run.setItalic(true);
            run.setFontSize(11);
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            run.setText("г. д. Балапанов К. К.                                                  ");
            paragraph.setAlignment(ParagraphAlignment.RIGHT);
            run.setText(employee.getSurname() + " " +
                    employee.getName().substring(0,1) + ". " +
                    employee.getFatherName().substring(0,1) + ".");


            // сохраняем модель docx документа в файл
            FileOutputStream outputStream = new FileOutputStream("src/main/resources/static/" +
                    employee.getName() + "_" + employee.getSurname() + "/intership.docx");
            document.write(outputStream);
            new PrinterService().printDoc(new File("src/main/resources/static/" +
                    employee.getName() + "_" + employee.getSurname() + "/intership.docx"));
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
