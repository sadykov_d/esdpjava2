package com.attractor.esdp.service;

import com.attractor.esdp.dao.EmployeeRepository;
import com.attractor.esdp.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class PhotoUploadService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Value("${spring.employee-images-storage}")
    private String path;

    public void uploadEmployeePhoto(MultipartFile multipartFile, Employee employee) throws Exception {

        String photoPath = path + employee.getId() + employee.getSurname() + employee.getName() + ".jpg";

        Files.write(Paths.get(photoPath), multipartFile.getBytes());

        employee.setPhoto("/empImages/" + employee.getId() + employee.getSurname() + employee.getName() + ".jpg");

        employeeRepository.save(employee);

    }

}