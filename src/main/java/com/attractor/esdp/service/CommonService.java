package com.attractor.esdp.service;

import java.util.List;

public interface CommonService<T> {
    T get(Integer id);

    T add(T entity);

    List<T> getAll();

    void update(T entity);

    void delete(Integer id);

    void delete(T entity);
}
