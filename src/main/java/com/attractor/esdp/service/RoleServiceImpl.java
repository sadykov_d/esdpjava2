package com.attractor.esdp.service;

import com.attractor.esdp.dao.RoleRepository;
import com.attractor.esdp.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl extends CommonServiceImpl<Role> implements RoleService {
private final RoleRepository roleRepository;
@Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role getRoleByName(String roleName) {
        return roleRepository.getByRoleName(roleName);
    }


}
