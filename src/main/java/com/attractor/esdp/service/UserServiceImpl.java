package com.attractor.esdp.service;

import com.attractor.esdp.dao.UserRepository;
import com.attractor.esdp.model.Role;
import com.attractor.esdp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl extends CommonServiceImpl<User> implements UserService {
    private final UserRepository userRepository;
    /*private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);*/
    @Autowired
    private PasswordEncoder passwordEncoder;


    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getByRole(Role role) {
        return userRepository.getUserByRole(role);
    }

    @Override
    public User get(Integer id) {
        return userRepository.getOne(id);
    }

    @Override
    public User add(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.saveAndFlush(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public void update(User entity) {
       userRepository.save(entity);
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public void delete(User entity) {

    }

    @Override
    public Optional<User> getByUsername(String username) {
        return userRepository.getByUsername(username);
    }
}
