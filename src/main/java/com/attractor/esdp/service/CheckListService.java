package com.attractor.esdp.service;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Set;

@Service
public class CheckListService {

    public void getCheckDoc(Set<String> list){

            new File("src/main/resources/static/checklist").mkdirs();

            try {
                XWPFDocument document = new XWPFDocument();

                XWPFParagraph paragraph = document.createParagraph();
                XWPFRun run = paragraph.createRun();

                // Head
                paragraph.setAlignment(ParagraphAlignment.CENTER);
                run.setBold(true);
                run.setFontSize(18);
                run.setText("Перечень документов");
                paragraph.setSpacingAfter(500);

                for (String i: list){
                    paragraph = document.createParagraph();
                    run = paragraph.createRun();
                    paragraph.setAlignment(ParagraphAlignment.LEFT);
                    run.setBold(false);
                    run.setText(i);
                    paragraph.setSpacingAfter(200);
                }

                // сохраняем модель docx документа в файл
                File file = new File("src/main/resources/static/checklist/check.docx");
                FileOutputStream outputStream = new FileOutputStream(file);

                document.write(outputStream);

                new PrinterService().printDoc(file); //send for printer
                if (file.exists())
                    file.delete();
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

