package com.attractor.esdp.exceptions;

public class NotFoundCertificateForGivenDateException extends RuntimeException {

    public NotFoundCertificateForGivenDateException() {
        super("Не найдены сертификаты по указаной дате");
    }

}
